/**
 * Importamos los primeros modulos que nos provee la API de Node
 * en este caso haremos las pruebas con el modulo File System y su metodo
 * writeFileSync
 */

 /**
  * Ejercicio: Añadir mensajes a nuestro archivo notes.js
  *     1. Usar appendFileSync para añadir el texto al ya existente
  *     2. Ejecutamos el Script
  *     3. Comprobar que se ha añadido correctamente
  *     ---------------------------------------------
  *     4. Probar a usar writeFile combinado con appendFileSync
  *     5. Probar a usar writeFile combinado con appendFile
  *     6.Probar a usar writeFile con un texto muy largo combinado con appendFileSync con un texto de una sola palabra
  */